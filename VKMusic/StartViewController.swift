//
//  StartViewController.swift
//  VKMusic
//


import UIKit

final class StartViewController: UIViewController {

    override func viewDidLoad() {
        navigationItem.title? = "Login"
    }
    
    @IBAction func loginAction(sender: AnyObject) {
        RequestManager.sharedManager.authorizeUser {
            LoginManager.sharedManager.login()
        }
    }
}
