//
//  SavedAudio.swift
//  VKMusic
//


import Foundation
import RealmSwift

class SavedAudio: Object {
    dynamic var url         = ""
    dynamic var title       = ""
    dynamic var artist      = ""
    dynamic var duration    = 0
}
